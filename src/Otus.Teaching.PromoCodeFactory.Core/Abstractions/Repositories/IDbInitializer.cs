﻿
namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IDbInitializer
    {
        void InitializeDB();
    }
}
